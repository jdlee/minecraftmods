/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.okcjug.minecraft;

import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 *
 * @author jdlee
 */
public class CreeperReinforcements {

    @SubscribeEvent
    public void spawnReinforcements(LivingDeathEvent event) {
        if (event.entity instanceof EntityCreeper) {
            for (int i = 0; i < 5; i++) {
                EntityCreeper creeper = new EntityCreeper(event.entity.worldObj);
                creeper.setLocationAndAngles(event.entity.posX, event.entity.posY, event.entity.posZ, 0, 0);

                if (!event.entity.worldObj.isRemote) {
                    event.entity.worldObj.spawnEntityInWorld(creeper);
                }
            }
        }
    }
    
    @SubscribeEvent
    public void deathEvent(LivingDeathEvent event) {
        event.entity.addChatMessage
                (new ChatComponentText(EnumChatFormatting.RED + 
                        "You killed a " + event.entity.getName()));
    }
}
