/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.okcjug.minecraft;

import net.minecraft.entity.item.EntityMinecart;
import net.minecraftforge.event.entity.minecart.MinecartCollisionEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 *
 * @author jdlee
 */
public class ExplodingMinecarts {
    
    @SubscribeEvent
    public void explode(MinecartCollisionEvent event) {
        EntityMinecart minecart = event.minecart;
        minecart.worldObj.createExplosion(minecart, minecart.posX, minecart.posY, minecart.posZ, 2, true);
    }
}
