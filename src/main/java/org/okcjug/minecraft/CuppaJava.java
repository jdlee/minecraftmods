/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.okcjug.minecraft;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

/**
 *
 * @author jdlee
 */
public class CuppaJava extends ItemFood {

    public CuppaJava() {
        super(1, 1.0f, true);
        setUnlocalizedName("cuppaJava");
        setCreativeTab(CreativeTabs.tabFood);
        setPotionEffect(Potion.moveSpeed.id, 60, 2, 1f);
        setAlwaysEdible();
    }
}