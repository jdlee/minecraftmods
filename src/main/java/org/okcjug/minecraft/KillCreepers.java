package org.okcjug.minecraft;

import com.google.common.base.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.minecraft.command.CommandException;
import net.minecraft.command.CommandKill;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

public class KillCreepers extends CommandKill { //implements ICommand {

    private final List aliases = new ArrayList();

    public KillCreepers() {
        aliases.add("killcreepers");
        aliases.add("kc");
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return "/killcreepers";
    }

    @Override
    public boolean isUsernameIndex(String[] args, int index) {
        return false;
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List getAliases() {
        return aliases;
    }

    @Override
    public void execute(ICommandSender sender, String[] args)
            throws CommandException {
        BlockPos pos = sender.getPosition();
        List<EntityCreeper> creepers = (List<EntityCreeper>) sender.getEntityWorld().getEntities(EntityCreeper.class,
                new Predicate() {

                    @Override
                    public boolean apply(Object input) {
                        return input instanceof EntityCreeper;
                    }

                });
        for (EntityCreeper creeper : creepers) {
            creeper.onKillCommand();
        }

//        CommandKill kill = new CommandKill();
//        kill.execute(sender, new String[]{"kill", "@e[type=Creeper]"});
    }

    private void sendErrorMessage(ICommandSender sender, String message) {
        sender.addChatMessage(new ChatComponentText(EnumChatFormatting.DARK_RED
                + message));
    }

    @Override
    public boolean canCommandSenderUse(ICommandSender sender) {
        return sender instanceof EntityPlayer;
    }

    @Override
    public List addTabCompletionOptions(ICommandSender sender, String[] args,
            BlockPos pos) {
        return null;
    }

}
