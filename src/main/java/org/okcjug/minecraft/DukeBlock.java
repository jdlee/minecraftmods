/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.okcjug.minecraft;

import java.util.List;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class DukeBlock extends Block {

    public DukeBlock() {
        super(Material.grass);
        setUnlocalizedName("dukeBlock");
        setCreativeTab(CreativeTabs.tabBlock);
        setResistance(1.0f);
        setHardness(1.0f);
        setLightLevel(1.0f);
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return ExampleMod.cuppaJava;
    }

    @Override
    public int quantityDropped(Random random) {
        return random.nextInt(2) + 3;
    }
}
