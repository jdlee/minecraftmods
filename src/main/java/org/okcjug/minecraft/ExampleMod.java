package org.okcjug.minecraft;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid = ExampleMod.MODID, version = ExampleMod.VERSION)
public class ExampleMod {

    public static final String MODID = "examplemod";
    public static final String VERSION = "1.0";

    public static Block dukeBlock;
    public static CuppaJava cuppaJava;

    @EventHandler
    public void init(FMLInitializationEvent event) {
        registerBlockBreakMessage();
        registerExplodingMinecarts();
        registerBiggerTntExplosion();
        registerCreeperReinforcements();
        registerCuppaJava();
        registerDukeBlock();
    }

    private void registerDukeBlock() {
        dukeBlock = new DukeBlock();

        GameRegistry.registerBlock(dukeBlock, "dukeBlock");

        Item dukeBlockItem = GameRegistry.findItem("examplemod", "dukeBlock");
        ModelResourceLocation dukeBlockModel = new ModelResourceLocation("examplemod:dukeBlock", "inventory");
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(dukeBlockItem, 0, dukeBlockModel);
    }

    private void registerCuppaJava() {
        cuppaJava = new CuppaJava();
        GameRegistry.registerItem(cuppaJava, "cuppaJava");

        Item cuppaJavaItem = GameRegistry.findItem("examplemod", "cuppaJava");
        ModelResourceLocation cuppaJavaModel = new ModelResourceLocation("examplemod:cuppaJava", "inventory");
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(cuppaJavaItem, 0, cuppaJavaModel);

    }

    private void registerCreeperReinforcements() {
        MinecraftForge.EVENT_BUS.register(new CreeperReinforcements());
    }

    private void registerBiggerTntExplosion() {
        MinecraftForge.EVENT_BUS.register(new BiggerTntExplosion());
    }

    private void registerExplodingMinecarts() {
        MinecraftForge.EVENT_BUS.register(new ExplodingMinecarts());
    }

    private void registerBlockBreakMessage() {
        MinecraftForge.EVENT_BUS.register(new BlockBreakMessage());
    }

    @EventHandler
    public void registerCommands(FMLServerStartingEvent event) {
        event.registerServerCommand(new KillCreepers());
    }
}
