/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.okcjug.minecraft;

import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.event.world.BlockEvent.BreakEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

/**
 *
 * @author jdlee
 */
public class BlockBreakMessage {
    @SubscribeEvent
    public void sendMEssage(BreakEvent event) {
        
        event.getPlayer()
                .addChatComponentMessage(new ChatComponentText(
                        EnumChatFormatting.GOLD + 
                        "You broke a " + EnumChatFormatting.RED + event.state.getBlock().getLocalizedName()));
    }
}
